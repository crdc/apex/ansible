import tsdb.utils
import psycopg2

if __name__ == '__main__':

    centrifuge_columns = [['speed_rpm', 'DOUBLE PRECISION', 'NULL'],
                          ['speed_set_rpm', 'DOUBLE PRECISION', 'NULL'],
                          ['torque_Nm', 'DOUBLE PRECISION', 'NULL'],
                          ['gforce', 'DOUBLE PRECISION', 'NULL'],
                          ['gforce_set', 'DOUBLE PRECISION', 'NULL'],
                          ['vibration_volts', 'DOUBLE PRECISION', 'NULL']]

    calibration_columns = [['speed_motor_x0', 'DOUBLE PRECISION', 'NULL'],
                           ['speed_motor_x1', 'DOUBLE PRECISION', 'NULL'],
                           ['speed_set_motor_x0', 'DOUBLE PRECISION', 'NULL'],
                           ['speed_set_motor_x1', 'DOUBLE PRECISION', 'NULL'],
                           ['torque_motor_x0', 'DOUBLE PRECISION', 'NULL'],
                           ['torque_motor_x1', 'DOUBLE PRECISION', 'NULL'],
                           ['gforce_x0', 'DOUBLE PRECISION', 'NULL'],
                           ['gforce_x1', 'DOUBLE PRECISION', 'NULL'],
                           ['gforce_set_x0', 'DOUBLE PRECISION', 'NULL'],
                           ['gforce_set_x1', 'DOUBLE PRECISION', 'NULL'],
                           ['vibration_motor_x0', 'DOUBLE PRECISION', 'NULL'],
                           ['vibration_motor_x1', 'DOUBLE PRECISION', 'NULL']]

    with psycopg2.connect(user='postgres',
                        host='localhost',
                        dbname='tsdb',
                        password='postgres') as con:

        tsdb.utils.create_ts_tbl_and_hdr_tbl('centrifuge', centrifuge_columns,
                                  'calibration', calibration_columns, con)





