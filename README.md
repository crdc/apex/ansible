# Apex Ansible Playbooks

Used to manage `apex` installations.

## Dependencies

Packages that should be installed are given here.

### Debian/Ubuntu

* `sudo`
* `python3`
* `python3-pip`
* `git`
* `python3-setuptools`
* `python3-wheel`
* `python3-psycopg2`
* `libpq-dev`
* `unzip`

## Quickstart

Simplified instructions to get going as soon as possible. This assumes
a non-root user has already been setup, and is used for these steps. If
not see the first steps of [Setup](#setup) before starting here.

```sh
mkdir ~/src
cd ~/src/
git clone https://gitlab.com/crdc/apex/ansible apex-ansible
cd apex-ansible
```

Add a `.env` file at this time if you have one.

Everything is configured to be installed in the `vars/main.yml` file during
the initial setup step, if something else should be configured check there
to toggle packages on or off.

```sh
./scripts/setup.sh
./scripts/install.sh
./scripts/schema.sh
```

## Setup

Executing the playbook steps as a non-root user is recommended.

```sh
useradd -d /home/oper -m -s /bin/bash -u 1000 -G sudo,plugdev oper
passwd oper
su - oper
mkdir -p .local/bin
```

If this is being done inside of a `systemd-nspawn` container that was created
using `mkosi` there are some additional steps that need to happen. For
containers created using `nspawn` an `/etc/hosts` file doesn't get created
because name resolution is meant to come from the underlying host using
`systemd-resolved` or similar. There are ways of doing name resolution
correctly, but that's outside of the scope of this document. The following is
sufficient.

```sh
sudo tee -a /etc/hosts >/dev/null <<EOF
127.0.0.1 localhost
::1       localhost
127.0.1.1 apex
EOF
```

It may also be possible to `systemctl enable systemd-resolved` and
`systemctl start systemd-resolved` to allow the host to handle this.

From here on it's expected that the user being run as is `oper`, or whatever was
used in the initial steps.

First clone this repository.

```sh
mkdir $HOME/src
cd $HOME/src
git clone https://gitlab.com/crdc/apex/ansible apex-ansible
```

For all following steps involving `ansible-playbook` the working directory is
assumed to be `$HOME/src/apex-ansible`.

### Container

This section is still a work in progress. The `ansible` connection plugin at
https://github.com/tomeon/ansible-connection-machinectl needs to be installed with:

```sh
ansible-galaxy install tomeon.ansible_connection_machinectl
mkdir -p ~/.ansible/plugins/connection
# not 100% sure about this
cp ~/.ansible/roles/tomeon.ansible_connection_machinectl/connection_plugins/machinectl.py \
  ~/.ansible/plugins/connection/
```

```sh
sudo -E ansible-playbook -i inventory/container -t container -K playbook.yml
```

### Python Dependencies

The version of `ansible` required is 2.4, if the repository being used doesn't
include a new enough version it should be installed using `pip`.

```sh
python3 -m pip install ansible
python3 -m pip install "python-dotenv[cli]"
```

### Ansible

This is an optional step if that is only necessary if the inventory file
included in this repository isn't right for you setup. Add a system hosts
configuration for Ansible using:

```sh
sudo mkdir -p /etc/ansible
sudo tee -a /etc/ansible/hosts >/dev/null <<EOF
[local]
master ansible_connection=local

[local:vars]
ansible_python_interpreter=/usr/bin/python3
EOF
```

Note that following calls to `ansible-playbook` should not include `-i inventory/local`
if this step was performed.

## Installation

In order to install the modules kept in private repositories you must obtain a
valid `.env` file. When you have this, place it in the same directory as this
`ansible` repository. Running the installation playbook with `dotenv` is not
necessary if you do not have a `.env` file for it to source, and will probably
create an error. An empty `.env` is sufficient as well and can be created with
`touch .env`.

### Environment

The first step should be to setup the `$PATH` that is taken care of by running
the `env.yml` playbook.

```sh
ansible-playbook -i inventory/local -K playbooks/env.yml
```

After this you must logout and login again for the changes to take effect, or
it is likely sufficient to run `source ~/.profile`.

### Requirements

The Ansible role requirements are listed in a file that is passed to
`ansible-galaxy`. If no other roles location has been configured for the
system or user then it may be necessary to allow the current user to save
roles to the system directory by giving write access to the `sudo` group.

```sh
sudo mkdir -p /etc/ansible/roles
sudo chgrp sudo /etc/ansible/roles
sudo chmod g+w /etc/ansible/roles
```

Installing the roles is done using:

```sh
dotenv run ansible-playbook -i inventory/local -K playbooks/galaxy.yml
```

### Configuration

There is a file `vars/main.yml` that controls what gets installed in the
playbook, this is mostly there to help during development. Underneath the
`# Role selection` comment just change `false` to `true` for anything that
should be installed.

### Cockpit

If `cockpit` should be installed on the host and Debian 9 is being used the
backports repository must be added.

```sh
echo 'deb http://deb.debian.org/debian stretch-backports main' | \
 sudo tee /etc/apt/sources.list.d/backports.list
sudo apt-get update
```

### Running the Playbook

Install everything that was selected during [configuration](#configuration).

```sh
dotenv run ansible-playbook -i inventory/local -K playbook.yml
dotenv run ansible-playbook -i inventory/local -K playbooks/modules.yml
```

The call to `dotenv` may fail if the locale is incorrect, to proceed it is
possible to manually source `.env` and run the playbook using

```sh
source .env
ansible-playbook -i inventory/local -K playbook.yml
ansible-playbook -i inventory/local -K playbooks/modules.yml
```

## Post Installation

### Databases

#### PostgreSQL

In order to restore from the backup the authentication needs to be set to `md5`.

```sh
sudo sed -i 's/^\(.* postgres\s*\)peer/\1md5/' /etc/postgresql/11/main/pg_hba.conf
sudo systemctl restart postgresql
```

#### Schema Recovery

```sh
pg_restore -U postgres -d postgres -v backup/postgres/pg.dump
mongorestore backup/mongodb/dump
```

## Check

The following should be sufficient to check if everything that has been
installed is now running.

```sh
systemctl status cockpit
systemctl status postgresql
systemctl status mongodb
systemctl status plantd-master
systemctl status plantd-configure
systemctl status plantd-unit@acquire
systemctl status plantd-unit@analyze
systemctl status plantd-unit@control
systemctl status plantd-unit@record
systemctl status plantd-unit@state
systemctl status plantd-module@acquire-genicam
systemctl status plantd-module@analyze-cant
systemctl status plantd-module@control-sinamics
systemctl status plantd-module@record-tsdb
systemctl status plantd-module@state-cent
```

## ToDo

* [x] Setup operator user (for now done manually)
* [x] Figure out how to source profile after adding GOPATH/GOROOT
* [x] Set password on postgresql database
* [x] Figure out how to run the database setup queries
* [x] Add nginx setup to the apex/master role
* [x] Add handlers to restart nginx after configuration
* [x] Add HTTPS to nginx configuration
* [x] Add apex-master vars for user vs. root installation
* [x] Install `cockpit`
* [x] Add a service handler for `cockpit`
* [x] Complete installation of modules
* [ ] Add an automated check to verify installation
* [x] Add role to install `apex-web`
* [x] Automate PostgreSQL password setup
* [ ] Automate PostgreSQL and MongoDB recovery
